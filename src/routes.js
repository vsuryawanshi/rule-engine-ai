import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import App from './containers/App';
import Home from './containers/Home';
import NotFoundPage from './containers/NotFoundPage.js';
import LoginPage from "./containers/Login";

export default (
    <Route>
        <Route path="/" component={App}>
            <IndexRedirect to="/login" />
            <Route path="login" component={LoginPage}/>
            <Route path="home" component={Home} />
            <Route path="*" component={NotFoundPage}/>
        </Route>
  </Route>
);
