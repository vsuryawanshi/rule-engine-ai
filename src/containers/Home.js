import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';


export default class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="homepage-wrapper">
                <header className="dt-header">
                    <div className="menu-toggle-wrap">
                    <IconButton>
                        <NavigationMenu />
                    </IconButton>
                    </div>
                </header>
            </div>
        );
    }
}