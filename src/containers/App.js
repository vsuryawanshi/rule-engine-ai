import React, { Component } from 'react'

//Main Application Wrapper
export default class App extends Component {
    render(){
        return (
            <div className="page-wrap">
                {this.props.children}
            </div>    
        );
    }
}