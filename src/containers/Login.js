import React, {Component} from "react";
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';


export default class LoginPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            userid : "",
            password : "",
            cardError:false,
            loading:false,
            cardSuccess:false,
            showErrorMsg:false
        }
    }

    handleLogin(){
        this.setState({
            loading:true,
            showErrorMsg:false
        });
        setTimeout(()=>{
            if(this.state.userid == "qwer"){
                this.props.router.push("home");
            } else {
                this.setState({
                    cardError:true,
                    loading:false,
                    showErrorMsg:true
                });
                setTimeout(()=>{
                    this.setState({
                        cardError:false
                    })
                },500);
            }
        },1000)
    }

    render(){
        return(
            <div className="login-wrap" style={{"width":window.innerWidth+"px", "height":window.innerHeight+"px"}}>
                <div className={`card` + (this.state.cardError ? " error" : "") + (this.state.cardSuccess ? " success" : "")}>
                    <div className="card-header">
                        <div className="company">
                            <i>mina</i> Rule Engine
                        </div>
                        <div className="sub">
                            Please login to continue
                        </div>
                    </div>
                    <div className="login-form">
                        <TextField
                            hintText="Enter your username"
                            floatingLabelText="username"
                            value={this.state.userid}
                            fullWidth={true}
                            onChange={(event)=>{
                                this.setState({userid : event.target.value});
                            }}/>
                        <TextField
                            hintText="Enter your password"
                            floatingLabelText="password"
                            value={this.state.password}
                            fullWidth={true}
                            type="password"
                            onChange={(event)=>{
                                this.setState({password : event.target.value});
                            }}/>
                        <div className="login-btn-wrap">
                            <RaisedButton 
                                label="Login" 
                                secondary={true} 
                                className="login-btn" 
                                disabled={this.state.userid == "" || this.state.password == ""}
                                onClick={this.handleLogin.bind(this)}/>
                        </div>
                        {
                            this.state.showErrorMsg ?
                                <div className="error">
                                    Please try again.
                                </div>
                            :
                            null
                        }
                    </div>
                    <div className={`cprog-wrap` + (this.state.loading ? " show" : "")}>
                        <CircularProgress size={60} thickness={7} />
                    </div>
                </div>
            </div>
        )
    }
}