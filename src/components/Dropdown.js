import React, {Component} from "react";

export default class Dropdown extends Component{
    constructor(props){
        super(props);
        this.state={
            selectedItemIndex:0,
            menuOpen:false
        }
    }

    menuItemChanged(selectedMenuItem, clickedIndex){
        this.setState({
            selectedItemIndex:clickedIndex,
            menuOpen:false    
        });
        this.props.onItemSelected(selectedMenuItem.value); 
    }

    render(){
        return(
            <div className="dropdown__wrap">
                <div className={`dropdown__mask` + (this.state.menuOpen ? " active" : "")} onClick={(event)=>{
                    event.preventDefault();
                    event.stopPropagation();
                    this.setState({menuOpen:false});
                }}/>
                <div className="dropdown__header" onClick={(event)=>{
                    event.preventDefault();
                    event.stopPropagation();
                    this.setState({menuOpen:!this.state.menuOpen});
                }}>
                    <img className="dropdown__header__img" src={require("../images/small-calendar.svg")}/>
                    <div className="dropdown__selected__label">
                        {this.props.items[this.state.selectedItemIndex].label}
                    </div>
                    <div className="dropdown__arrow"/>
                </div>
                <div className={`dropdown__menu` + (this.state.menuOpen ? " open" : "")}>
                    {
                        this.props.items.map((menuItem, index)=>{
                            return(
                                <div key={index} className={`menu__item` + (this.state.selectedItemIndex == index ? " selected" : "")} onClick={(event)=>{
                                    event.preventDefault();
                                    event.stopPropagation();
                                    this.menuItemChanged(menuItem,index);
                                }}>
                                    {menuItem.label}
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}