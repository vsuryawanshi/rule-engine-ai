import React, {Component}  from "react";
import axios from "axios";

const AUTOCOMPLETE_URL = `http://62.210.248.133:8000/api/citySuggest?name=`;

export default class Autocomplete extends Component{
    constructor(props){
        super(props);
        this.state = {
            autocompleteActive:false,
            queryText:"",
            queryResults:[]
        }
    }

    clearQuery(){
        this.setState({
            queryText:"",
            queryResults:[],
            autocompleteActive:false
        });
    }

    handleChange(currentQuery){
        this.setState({
            queryText:currentQuery
        });
        this.props.onQueryTextChanged(currentQuery);
        if(currentQuery !== null && currentQuery != ""){
            axios.get(AUTOCOMPLETE_URL+currentQuery).then((response)=>{
                this.setState({
                    autocompleteActive:true,
                    queryResults:response.data.results
                });
            }).catch((error)=>{
                console.error(error);
            })
        } else {
            this.setState({
                autocompleteActive:false,
                queryResults:[]
            })
        }
    }

    onItemSelected(selectedResult){
        this.setState({
            queryText:selectedResult.fullName,
            autocompleteActive:false
        });
        this.props.onItemSelected(selectedResult);
    }

    render(){
        return(
            <div className="autocomplete__wrap">
                <div className={`autocomplete__mask` + (this.state.autocompleteActive ? " active" : "")} onClick={(event)=>{
                    event.preventDefault();
                    event.stopPropagation();
                    this.setState({autocompleteActive:false});
                }}/>
                <div className="autocomplete__header">
                    <input className="autocomplete__text" value={this.state.queryText} placeholder={this.props.placeholder} onChange={(event)=>{
                        this.handleChange(event.target.value);
                    }} onFocus={(event)=>{
                        this.setState({autocompleteActive:true})
                    }}/>
                    <button className={`autocomplete__clear` + (this.state.queryText == "" ? " hidden" : "")} onClick={(event)=>{
                        this.clearQuery();
                    }}>
                        <img src={require("../images/clear-button.svg")}/>
                    </button>
                </div>
                <div className={`autocomplete__results` + (this.state.autocompleteActive ? " shown" : "")}>
                    {
                        this.state.queryResults.length > 0 ?
                        <div className={`results-wrap`}>
                            {
                                this.state.queryResults.map((searchResult,index)=>{
                                    return(
                                        <div className="result-item" key={index} onClick={(event)=>{
                                            event.preventDefault();
                                            event.stopPropagation();
                                            this.onItemSelected(searchResult);
                                        }}>
                                            {searchResult.fullName}
                                        </div>
                                    );
                                })
                            }
                        </div>
                        :
                        <div className="no-results">
                            No results to show
                        </div>
                    }
                </div>
            </div>
        );
    }
}